<?php

class Api
{
  protected $startTag;
  protected $endTag;

  public function __construct($startTag = "%", $endTag = "%")
  {
    $this->startTag = $startTag;
    $this->endTag = $endTag;
  }

  /**
   * Заполняет строковый шаблон template данными из объекта object
   *
   * @author		User Name
   * @version		v.1.0 (dd/mm/yyyy)
   * @param		array $array
   * @param		string $template
   * @return		string
   */
  public function get_api_path(array $array, string $template) : string
  {
    $result = '';

    $result = $template;

    foreach($array as $key => $value) {
      $result = str_replace($this->startTag.$key.$this->endTag, rawurlencode($array[$key]), $result);
    }

    return $result;
  }
}

$user =
[
  'id'      => 20,
  'name'    => 'John Dow',
  'role'    => 'QA',
  'salary'  => 100
];

$api_path_templates =
[
  "/api/items/%id%/%name%",
  "/api/items/%id%/%role%",
  "/api/items/%id%/%salary%"
];

$api = new Api();

$api_paths = array_map(function ($api_path_template) use ($api, $user)
{
  return $api->get_api_path($user, $api_path_template);
}, $api_path_templates);

echo (json_encode($api_paths, JSON_UNESCAPED_SLASHES));
